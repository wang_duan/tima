// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
	let data = {
		...event.config
	}
	delete data._id
	if(event.action=='dele'){
		await db.collection('user-works').doc(event.id).remove()
		return '删除成功'
	}else{
		const result = await db.collection("user-works").add({
			data,
		})
		return result._id
	}
}
