## 部署

1. 克隆代码到本地
2. 修改代码的appid
3. 开通小程序云开发
4. 直接运行计科

## 项目截图
 ![小程序首页](https://images.gitee.com/uploads/images/2021/1012/195533_001fef56_5229673.jpeg "Screenshot_2021-10-12-19-53-09-896_com.tencent.mm.jpg")
 ![小程序模板页](https://images.gitee.com/uploads/images/2021/1012/195605_69cc7d5e_5229673.jpeg "Screenshot_2021-10-12-19-53-21-959_com.tencent.mm.jpg")
 ![小程序个人页](https://images.gitee.com/uploads/images/2021/1012/195712_34f0e423_5229673.jpeg "Screenshot_2021-10-12-19-53-50-647_com.tencent.mm.jpg")

## 项目预览 

 ![微信小程序码](https://images.gitee.com/uploads/images/2021/1012/195909_72f035f8_5229673.jpeg "gh_8d07bf8c4da4_258.jpg")