export default  {
	xs: 24,
	sm: 26,
	smx:28,
	md: 30,
	mdx: 32,
	lg:34,
	lgx:36,
	xl: 38,
}