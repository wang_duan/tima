/** 
 * 提供模板测试数据
 * 随机人物图片：https://api.pingping6.com/tools/acg3/
 * 随机风景图片：https://api.pingping6.com/tools/scenery/index.php
 * 
 * */
// 倒计时
export const timeDiff = (s, e) => {
	const sta = Date.parse(s)
	const end = Date.parse(e)
	const day = (end - sta) / (1 * 24 * 60 * 60 * 1000)
	return day
}
// 制作模板时的假数据
export const config = {
	"is_template": true,
	"tempId": 2.0,
	"temp": {
		"template": "page06",
		"cover": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/cloudbase-cms/upload/2021-05-16/ijk1i8elerf7ui8rrru7rsqtrnhyx3sq_.jpg",
		"info": "模板：热烈庆祝中国共产党建党100周年",
		"avatar": "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2207ea21-324c-4b83-b988-617215f5fd2a/d0130ce4-e1c4-4e65-9cac-18e886cabd15.png",
		"time": "2021-4-28"
	},
	"bg": {
		"prompt": "背景图片",
		"edit": true,
		"info": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/cloudbase-cms/upload/2021-05-16/ijk1i8elerf7ui8rrru7rsqtrnhyx3sq_.jpg",
		"key": "bg",
		"type": "image"
	},
	"music": {
		"prompt": "背景音乐",
		"edit": true,
		"info": "https://lime-1257272319.cos.ap-chongqing.myqcloud.com/love/1612676953070.mp3",
		"key": "music",
		"title": "那些年",
		"type": "music"
	},
	"text0": {
		"prompt": "第一个页面的标题",
		"edit": true,
		"info": "1921-2021",
		"key": "text0",
		"type": "text"
	},
	"text1": {
		"prompt": "第一个页面的文字",
		"edit": true,
		"info": "100周年。城市唱，乡村和，祖国处处都是歌。歌唱建党一百年，惠风泽雨润山河。改革开放穷变富，民族振兴扬特色，党群一致谱新歌。",
		"key": "text1",
		"type": "text"
	},
	"text2": {
		"prompt": "第二个页面的标题",
		"edit": true,
		"info": "为建党100周年送祝福",
		"key": "text2",
		"type": "text"
	},
	"text3": {
		"prompt": "第二个页面的文字",
		"edit": true,
		"info": "100周年。城市唱，乡村和，祖国处处都是歌。歌唱建党一百年，惠风泽雨润山河。改革开放穷变富，民族振兴扬特色，党群一致谱新歌。",
		"key": "text3",
		"type": "text"
	},
	"text4": {
		"prompt": "第三个页面的标题",
		"edit": true,
		"info": "为建党100周年送祝福",
		"key": "text4",
		"type": "text"
	},
	"text5": {
		"prompt": "第三个页面的文字",
		"edit": true,
		"info": "100周年。城市唱，乡村和，祖国处处都是歌。歌唱建党一百年，惠风泽雨润山河。改革开放穷变富，民族振兴扬特色，党群一致谱新歌。",
		"key": "text5",
		"type": "text"
	},
	"text6": {
		"prompt": "第四个页面的标题",
		"edit": true,
		"info": "为建党100周年送祝福",
		"key": "text6",
		"type": "text"
	},
	"text7": {
		"prompt": "第四个页面的文字",
		"edit": true,
		"info": "100周年。城市唱，乡村和，祖国处处都是歌。歌唱建党一百年，惠风泽雨润山河。改革开放穷变富，民族振兴扬特色，党群一致谱新歌。",
		"key": "text7",
		"type": "text"
	},
	"image0": {
		"prompt": "第一个图片",
		"edit": true,
		"info": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/avatar/25876dd2e1d9936ca7c80d53cf935601.png?sign=1ddf8b13bb6657681276f816ed0e3698&t=1621170190",
		"key": "image0",
		"type": "image"
	},
	"image1": {
		"prompt": "第二个图片",
		"edit": true,
		"info": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/avatar/25876dd2e1d9936ca7c80d53cf935601.png?sign=1ddf8b13bb6657681276f816ed0e3698&t=1621170190",
		"key": "image1",
		"type": "image"
	},
	"image2": {
		"prompt": "第三个图片",
		"edit": true,
		"info": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/avatar/25876dd2e1d9936ca7c80d53cf935601.png?sign=1ddf8b13bb6657681276f816ed0e3698&t=1621170190",
		"key": "image2",
		"type": "image"
	},
	"image3": {
		"prompt": "第四个图片",
		"edit": true,
		"info": "https://6262-bbq-7bpgj-1300588877.tcb.qcloud.la/avatar/25876dd2e1d9936ca7c80d53cf935601.png?sign=1ddf8b13bb6657681276f816ed0e3698&t=1621170190",
		"key": "image3",
		"type": "image"
	}
}
// 进入小程序初始化内容
export const appInit = () => {
	// 初始化腾讯云函数
	wx.cloud.init()
	// 初始化小程序更新
	const updateManager = uni.getUpdateManager();
	updateManager.onCheckForUpdate();
	updateManager.onUpdateReady(function(res) {
		uni.showModal({
			title: '更新提示',
			content: '新版本已经准备好，是否重启应用？',
			showCancel: false,
			success(res) {
				updateManager.applyUpdate();
			}
		});
	});
	updateManager.onUpdateFailed(function(res) {
		uni.showToast({
			title: '新版本下载失败，请手动重启尝试',
			duration: 2000,
			icon: "none"
		});
	});
}
